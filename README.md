cowsay
 __________________
< srsly dude, why? >
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
cowsay is a configurable talking cow, originally written in Perl by Tony Monroe

This project is a translation in JavaScript of the original program and an attempt to bring the same silliness to node.js.